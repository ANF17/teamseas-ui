import React, { useEffect, useRef } from "react";
import { animate } from "framer-motion";

interface Props {
  from: number;
  to: number;
}

function Counter({ from, to }: Props) {
  const nodeRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    const node = nodeRef.current;
    if (node) {
      const controls = animate(from, to, {
        duration: 1,
        onUpdate(value) {
          node.textContent = parseInt(value.toFixed(0)).toLocaleString();
        },
      });

      return () => controls.stop();
    }
  }, [from, to]);

  return <div ref={nodeRef} />;
}

export default Counter;
