import {
  Box,
  Heading,
  Radio,
  RadioGroup,
  Stack,
  VStack,
} from "@chakra-ui/react";
import React, { useState } from "react";
import LeaderboardItem from "./LeaderboardItem";
import { Donation } from "../types";
import { useQuery } from "urql";

const DonationsQuery = `
query Query($orderBy: OrderByParams) {
    donations(orderBy: $orderBy) {
      count
      id
      displayName
      createdAt
      message
      team  
    } 
  }
`;

type DonationQueryRes = {
  donations: Donation[];
};

function Leaderboard() {
  const [field, setOrderByField] = useState("createdAt");

  const [{ data, fetching, error }] = useQuery<DonationQueryRes>({
    query: DonationsQuery,
    variables: {
      orderBy: {
        field,
        direction: "desc",
      },
    },
  });

  if (fetching || !data) return <p>Loading...</p>;
  if (error) return <p>Error...{error.message}</p>;

  return (
    <Box w="100%">
      <VStack spacing={4}>
        <Heading as="h1" size="2xl">
          LEADERBOARD
        </Heading>

        <RadioGroup onChange={setOrderByField} value={field}>
          <Stack direction="row">
            <Radio value="createdAt">Most Recent</Radio>
            <Radio value="count">Most Pounds</Radio>
          </Stack>
        </RadioGroup>

        {data.donations.map((donation) => (
          <LeaderboardItem donation={donation} key={donation.id} />
        ))}
      </VStack>
    </Box>
  );
}

export default Leaderboard;
